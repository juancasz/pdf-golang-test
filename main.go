package main

import (
	"fmt"

	"pdf-test/codeGenerator"

	"github.com/jung-kurt/gofpdf"
	"github.com/jung-kurt/gofpdf/contrib/barcode"
)

func createPdf() (pdf *gofpdf.Fpdf) {
	pdf = gofpdf.New("P", "mm", "A4", "")
	pdf.SetFont("Helvetica", "", 12)
	pdf.SetFillColor(200, 200, 220)
	pdf.AddPage()
	return
}

//GenerateBarcodePDF generate and print a barcode into a PDF
//GTIN: 13 characters length string. All the characters must be numbers. Required
//referenceNumber: string from 2 to 24 characters. All the characters must be numbers. Required
//paymentValue: string from 2 to 12 characters. Decimal notation allowed (For example 123.123). Optional. If not needed pass parameter as an empty string
//maxPaymentDate: 8 characters length string. Format AAAAMMDD. Optional. If not needed pass parameter as an empty string
//width: mm width of the barcode
//height: mm height of the barcode
func GenerateBarcodePDF(GTIN string, referenceNumber string, paymentValue string, maxPaymentDate string, width float64, height float64) {
	code, err := codeGenerator.GenerateCode(GTIN, referenceNumber, paymentValue, maxPaymentDate)

	fmt.Println("Creating barcode:", code)

	if err != nil {
		fmt.Println("barcode can't be generated:", err)
		return
	}

	pdf := createPdf()

	//create and position the barcode
	xPositionBarCode := (210 - width) / 2
	key := barcode.RegisterCode128(pdf, code)
	barcode.Barcode(pdf, key, xPositionBarCode, 15, width, height, false)
	///////////

	pdf.SetFont("Arial", "", 10)
	pdf.SetY(15 + height)
	pdf.CellFormat(0, 5, code, "", 1, "C", false, 0, "")

	err = pdf.OutputFileAndClose("barcode.pdf")

	if err != nil {
		fmt.Println("can't save into file:", err)
		return
	}
}

func main() {
	fmt.Println("Creating PDF..")

	GTIN := "7701234000011"
	referenceNumber := "9876"
	paymentValue := "25500"
	maxPaymentDate := "20210605"
	//referenceNumber := "987619293912908919202132"
	//paymentValue := "25500143123"
	//maxPaymentDate := "20210605"
	//paymentValue := ""
	//maxPaymentDate := ""

	GenerateBarcodePDF(GTIN, referenceNumber, paymentValue, maxPaymentDate, 150, 20)
}
